/*
 * @Author: wupinshuo
 * @Date: 2022-03-24 13:27:02
 * @LastEditors: wupinshuo
 * @LastEditTime: 2022-03-24 13:40:42
 */
import { Body, Controller, Get, Post } from '@nestjs/common';
import { userTableDTO } from './usertable.dto';
import { UsertableService } from './usertable.service';

@Controller('travel')
export class UsertableController {
  constructor(private readonly usertableService: UsertableService) {}

  @Get('getAllUsers')
  public async getAllUsers(){
    return await this.usertableService.getAllUser();
  }

  @Post('login')
  public async login(@Body()req:userTableDTO.user){
    return await this.usertableService.login(req);
  }

  @Post('register')
  public async register(@Body()req:userTableDTO.user){
    return await this.usertableService.register(req);
  }
}
